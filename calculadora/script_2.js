//ECMA-Script 5: estandarización de JS desde 2000
//ECMA-Script 6: estandarización de JS desde 2015 (ECMA-Script 2015,ES6)
//Incluye soporte para clases, template strings, pero es lo mismo...
// var tiene ámbito de función, let tiene ámbito de bloque de instrucciones
class Boton{
    constructor(id,valor,clase,parent){
        let input = document.createElement("input");
        input.type = "button";
        input.value = valor;
        input.id = id;
        input.setAttribute("class",clase);
        parent.appendChild(input);
        this.input = input;
    }
}