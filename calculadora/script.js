var body = document.getElementsByTagName("body")[0];
/*
Opciones: usar objetos para ahorrar programación?
class BotonNum:
- Características
* Propiedades
* Atributos
* Variables
- Acciones
* Funcionalidad
* Funciones
* Método

Elemento DOM
- botón input
- id
- value
- CSS class
- parent
- función (método para evento click)
- constructor (para definir las variables y propiedades)
*/
/**/

//var buttons = document.getElementsByTagName("input");
var clear = true;
var prevOp = "";
var Memory = 0;
var buts = [
    {"id":"MemCl","clase":"op","valor":"MC"},
    {"id":"MemRc","clase":"op","valor":"MR"},
    {"id":"MemPl","clase":"op","valor":"M+"},
    {"id":"MemMn","clase":"op","valor":"M-"},
    {"id":"Perc","clase":"op","valor":"%"},
    {"id":"CE","clase":"op","valor":"CE"},
    {"id":"Clear","clase":"op","valor":"C"},
    {"id":"Erase","clase":"op","valor":"R"},
    {"id":"Inv","clase":"op","valor":"1/x"},
    {"id":"Square","clase":"op","valor":"x^2"},
    {"id":"SqRoot","clase":"op","valor":"sqr"},
    {"id":"Divis","clase":"op","valor":"/"},
    {"id":"Num7","clase":"num","valor":"7"},
    {"id":"Num8","clase":"num","valor":"8"},
    {"id":"Num9","clase":"num","valor":"9"},
    {"id":"Prod","clase":"op","valor":"*"},
    {"id":"Num4","clase":"num","valor":"4"},
    {"id":"Num5","clase":"num","valor":"5"},
    {"id":"Num6","clase":"num","valor":"6"},
    {"id":"Minus","clase":"op","valor":"-"},
    {"id":"Num1","clase":"num","valor":"1"},
    {"id":"Num2","clase":"num","valor":"2"},
    {"id":"Num3","clase":"num","valor":"3"},
    {"id":"Plus","clase":"op","valor":"+"},
    {"id":"PlMin","clase":"op","valor":"+-"},
    {"id":"NumDot","clase":"num","valor":"."},
    {"id":"Num0","clase":"num","valor":"0"},
    {"id":"Equal","clase":"op","valor":"="}
];

var buttons = [];
class Boton{
    constructor(id,valor,clase,parent){
        let input = document.createElement("input");
        input.type = "button";
        input.value = valor;
        input.id = id;
        input.clase = clase;
        input.setAttribute("class",clase);
        parent.appendChild(input);
        this.input = input;
    }
}
var padre = document.getElementById("botones");
for (b of buts){
    let id = b['id'];
    let clase = b['clase'];
    let valor = b['valor'];
    let btn = new Boton(id,valor,clase,padre);
    if(clase == "num"){
        btn.input.addEventListener("click",numero);
    }
    else if(clase == "op"){
        btn.input.addEventListener("click",operador)
    }
    buttons.push(btn);
}

function operacion(v1,v2,o){
    switch(o){
        case "+":
            return v1+v2;
            break;
        case "-":
            return v1-v2;
            break;
        case "*":
            return v1*v2;
            break;
        case "/":
            return v1/v2;
            break;
        case "sqr":
            return Math.sqrt(v1);
            break;
        case "%":
            return v1*v2/100.0;
            break;
        case "M+":
            Memory += v1;
            break;
        case "M-":
            Memory -= v1;
            break;
        case "MR":
            return Memory;
            break;
        case "MC":
            Memory = 0;
            break;
        default:
            return v2;
            break;
    }
}
function operador(){
    var values = document.getElementsByTagName("p");
    var resul = values[1];
    var oper = values[0];
    o = this.value;
    op = oper.innerHTML;
    vop = parseFloat(op);
    res = resul.innerHTML;
    vrr = parseFloat(res);
    console.log(vop + " " + res + " " + o);
    switch(o){
        case "C":
            res = "0";
            op = "0";
            prevOp = "";
            clear = true;
            break;
        case "R":
            res = res.substr(0,res.length-1);
            break;
        case "=":
            op = operacion(vop,vrr,prevOp);
            res = "0";
            prevOp = "";
            break;
        case "+-":
            res = operacion(-1,vrr,"*");
            break;
        case "1/x":
            res = operacion(1,vrr,"/");
            break;
        case "x^2":
            res = operacion(vrr,vrr,"*");
            break;
        case "sqr":
            res = operacion(vrr,0,o);
            break;
        case "%":
            res = operacion(vop,0.01*vrr,"*");
            break;
        case "M+":
            Memory += vrr;
            break;
        case "M-":
            Memory -= vrr;
            break;
        case "MC":
            Memory = 0;
            break;
        case "MR":
            res = Memory;
            break;
        default:
            if(res != ""){
                op = operacion(vop,vrr,prevOp);
                res = "0";
            }
            prevOp = o;
    }
    resul.innerHTML = res;
    oper.innerHTML = op;
}
function numero(){
    var resul = document.getElementById("resultado");
    var car = this.value;
    if(clear){
        clear = false;
        resul.innerHTML="";
    }
    resul.innerHTML+=car;
}